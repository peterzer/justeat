package peter.szucs.justeattest.data;

import peter.szucs.justeattest.data.model.RestaurantsRequest;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * ApiInterface for the Requests
 * Created by peterszucs on 10/11/15.
 */
interface ApiInterface {

    interface NODES {
        String RESTAURANTS = "/restaurants";
    }

    interface QUERIES {
        String SEARCH = "q";
    }


    @GET(ApiInterface.NODES.RESTAURANTS)
    Observable<RestaurantsRequest> getRestaurantsForOutcode(
            @Query(QUERIES.SEARCH) String outcode
    );


}
