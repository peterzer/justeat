package peter.szucs.justeattest.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Logo
 * Created by peter on 2015. 11. 12..
 */
public class Logo {

    @Getter
    @SerializedName("StandardResolutionURL")
    private String standardResolutionURL;

}
