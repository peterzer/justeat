package peter.szucs.justeattest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;

/**
 * RestaurantsRequest
 * Created by peter on 2015. 11. 13..
 */
public class RestaurantsRequest {

    @Getter
    @SerializedName("ShortResultText")
    private String shortResultText;

    @Getter
    @SerializedName("Restaurants")
    private List<Restaurant> restaurants;

}
