package peter.szucs.justeattest.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Deal
 * Created by peter on 2015. 11. 12..
 */
public class Deal {

    @Getter
    @SerializedName("Description")
    private String description;

    @Getter
    @SerializedName("DiscountPercent")
    private String discountPercent;

    @Getter
    @SerializedName("QualifyingPrice")
    private String qualifyingPrice;

}
