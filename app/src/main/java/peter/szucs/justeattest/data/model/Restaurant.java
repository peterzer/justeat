package peter.szucs.justeattest.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Restaurant
 * Created by peter on 2015. 11. 13..
 */
public class Restaurant {

    @Getter
    @SerializedName("Id")
    private String id;

    @Getter
    @SerializedName("Name")
    private String name;

    @Getter
    @SerializedName("Address")
    private String address;

    @Getter
    @SerializedName("Postcode")
    private String postcode;

    @Getter
    @SerializedName("City")
    private String city;

    @Getter
    @SerializedName("CuisineTypes")
    private CuisineType[] cuisineTypes;

    @Getter
    @SerializedName("Url")
    private String url;

    @Getter
    @SerializedName("IsOpenNow")
    private boolean openNow;

    @Getter
    @SerializedName("IsSponsored")
    private boolean sponsored;

    @Getter
    @SerializedName("IsNew")
    private boolean newRestaurant;

    @Getter
    @SerializedName("IsTemporarilyOffline")
    private boolean temporarilyOffline;

    @Getter
    @SerializedName("ReasonWhyTemporarilyOffline")
    private String reasonWhyTemporarilyOffline;

    @Getter
    @SerializedName("UniqueName")
    private String uniqueName;

    @Getter
    @SerializedName("IsCloseBy")
    private boolean closedBy;

    @Getter
    @SerializedName("IsHalal")
    private boolean isHalal;

    @Getter
    @SerializedName("DefaultDisplayRank")
    private int defaultDisplayRank;

    @Getter
    @SerializedName("NumberOfRatings")
    private int numberOfRatings;

    @Getter
    @SerializedName("IsOpenNowForDelivery")
    private boolean isOpenNowForDelivery;

    @Getter
    @SerializedName("IsOpenNowForCollection")
    private boolean isOpenNowForCollection;

    @Getter
    @SerializedName("RatingStars")
    private float ratingStars;

    @Getter
    @SerializedName("Logo")
    private Logo[] logos;

    @Getter
    @SerializedName("Deals")
    private Deal[] deals;

}
