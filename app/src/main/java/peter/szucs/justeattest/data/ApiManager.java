package peter.szucs.justeattest.data;

import android.content.Context;

import java.util.List;

import lombok.Getter;
import peter.szucs.justeattest.BuildConfig;
import peter.szucs.justeattest.R;
import peter.szucs.justeattest.data.model.Restaurant;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * ApiManager - handles the API through retrofit, and its ApiInterface
 * Created by peter on 2015. 11. 12..
 */
public class ApiManager {

    enum Headers {
        ACCEPT_TENANT(R.string.header_key_accept_tenant, R.string.header_value_accept_tenant),
        ACCEPT_LANGUAGE(R.string.header_key_accept_language, R.string.header_value_accept_language),
        AUTHORIZATION(R.string.header_key_authorization, R.string.header_value_authorization),
        HOST(R.string.header_key_host, R.string.header_value_host);

        @Getter
        final int keyResourceCode;
        @Getter
        final int valueResourceCode;

        Headers(final int keyResourceCode, final int valueResourceCode) {
            this.keyResourceCode = keyResourceCode;
            this.valueResourceCode = valueResourceCode;
        }
    }

    private final String host;

    private ApiInterface apiInterface;
    private final Context context;

    public ApiManager(Context context) {
        this.context = context;
        host = getString(R.string.api_host);
        createRetrofit();
    }

    private void createRetrofit() {
        RequestInterceptor requestInterceptor = request -> {
            for (Headers header : Headers.values()) {
                request.addHeader(getString(header.getKeyResourceCode()),
                        getString(header.getValueResourceCode()));
            }
        };

        RestAdapter retrofit = new RestAdapter.Builder()
                .setEndpoint(host)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setRequestInterceptor(requestInterceptor)
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    private String getString(int resourceId) {
        return context.getString(resourceId);
    }

    public Observable<List<Restaurant>> getRestaurantsForOutcode(String outcode) {
        return apiInterface.getRestaurantsForOutcode(outcode)
                .observeOn(Schedulers.io())
                .flatMap(restaurantsRequest -> Observable.just(restaurantsRequest.getRestaurants()));
    }

}
