package peter.szucs.justeattest.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * CuisineType
 * Created by peter on 2015. 11. 12..
 */
public class CuisineType {

    @Getter
    @SerializedName("Id")
    private int id;

    @Getter
    @SerializedName("Name")
    private String name;

    @Getter
    @SerializedName("SeoName")
    private String seoName;

}
