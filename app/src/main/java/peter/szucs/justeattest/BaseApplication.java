package peter.szucs.justeattest;

import android.app.Application;

import lombok.Getter;
import peter.szucs.justeattest.di.ApplicationComponent;
import peter.szucs.justeattest.di.ApplicationModule;
import peter.szucs.justeattest.di.DaggerApplicationComponent;

/**
 * Just Eat Test Application
 * Created by peter on 2015. 11. 12..
 */
public class BaseApplication extends Application {

    @Getter
    private static BaseApplication instance;

    @Getter
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initializeDagger();
    }

    private void initializeDagger() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }


}
