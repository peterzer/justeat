package peter.szucs.justeattest.view.main.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import peter.szucs.justeattest.R;

/**
 * ViewHolder Pattern for the Restaurant Object
 * Created by peter on 2015. 11. 13..
 */
public class RestaurantViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.restitem_logo)
    public ImageView logo;

    @Bind(R.id.restitem_name)
    public TextView name;

    @Bind(R.id.restitem_rating)
    public TextView rating;

    @Bind(R.id.restitem_type)
    public TextView type;

    public RestaurantViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
