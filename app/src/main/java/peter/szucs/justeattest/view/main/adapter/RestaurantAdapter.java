package peter.szucs.justeattest.view.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import peter.szucs.justeattest.R;
import peter.szucs.justeattest.data.model.CuisineType;
import peter.szucs.justeattest.data.model.Restaurant;
import peter.szucs.justeattest.view.main.viewholder.RestaurantViewHolder;

/**
 * Recycling the views.
 * Created by peter on 2015. 11. 13..
 */
public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

    private final List<Restaurant> items;
    private final Context context;

    public RestaurantAdapter(final Context context, final List<Restaurant> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_restaurant, parent, false);
        return new RestaurantViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, int position) {
        Restaurant restaurant = items.get(position);
        Picasso.with(context).load(restaurant.getLogos()[0].getStandardResolutionURL()).into(holder.logo); // I assume for now that every restaurant has at least one logo. (based on json)
        holder.name.setText(restaurant.getName());
        holder.rating.setText(String.valueOf(restaurant.getRatingStars()));

        String ctString = "";
        for (CuisineType ct : restaurant.getCuisineTypes()) {
            ctString += ct.getName() + ", ";
        }
        ctString = ctString.substring(0, ctString.lastIndexOf(","));
        holder.type.setText(ctString);
    }

    @Override
    public int getItemCount() {
        if (items == null) return 0;
        return items.size();
    }
}
