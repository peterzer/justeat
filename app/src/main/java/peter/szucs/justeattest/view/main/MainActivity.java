package peter.szucs.justeattest.view.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;
import peter.szucs.justeattest.R;
import peter.szucs.justeattest.data.model.Restaurant;
import peter.szucs.justeattest.view.base.BaseActivity;
import peter.szucs.justeattest.view.main.adapter.RestaurantAdapter;

public class MainActivity extends BaseActivity implements MainView {

    @Getter
    private MainPresenter mainPresenter;

    @Bind(R.id.input_outcode)
    EditText outcodeEt;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.restaurant_list)
    RecyclerView recyclerView;

    @Bind(R.id.no_results)
    TextView noResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mainPresenter = new MainPresenter(this, this);

        outcodeEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mainPresenter.requestData(outcodeEt.getText().toString().trim());
            }
            return false;
        });
    }

    @OnClick(R.id.my_post_code_button)
    public void onClickMyPostCodeButton() {
        mainPresenter.onClickGetMyPostCode();
    }

    @Override
    public void showProgress() {
        runOnUiThread(() ->
                    progressBar.setVisibility(View.VISIBLE)
        );
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() ->
                    progressBar.setVisibility(View.GONE)
        );
    }

    @Override
    public void dataArrived(List<Restaurant> restaurants) {
        runOnUiThread(() -> {
                    if (restaurants.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        noResults.setVisibility(View.VISIBLE);
                        return;
                    }
                    noResults.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.setAdapter(new RestaurantAdapter(this, restaurants));
                }
        );
    }

    @Override
    public void showMessage(String msg) {
        runOnUiThread(() ->
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show()
        );
    }

    @Override
    public void setPostalCodeFromLocation(String postalCode) {
        runOnUiThread(() -> outcodeEt.setText(postalCode));
    }
}
