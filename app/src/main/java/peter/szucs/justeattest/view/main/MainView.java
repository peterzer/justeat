package peter.szucs.justeattest.view.main;

import java.util.List;

import peter.szucs.justeattest.data.model.Restaurant;

/**
 * This is an interface for the view - so with it the view can be replaced anytime.
 * Created by peter on 2015. 11. 12..
 */
public interface MainView {

    void showProgress();

    void hideProgress();

    void dataArrived(List<Restaurant> restaurants);

    void showMessage(String msg);

    void setPostalCodeFromLocation(String postalCode);
}
