package peter.szucs.justeattest.view.base;

import android.content.Context;

import lombok.Getter;
import peter.szucs.justeattest.BaseApplication;
import peter.szucs.justeattest.di.ApplicationComponent;

/**
 * Generic BasePresenter
 * Created by peter on 2015. 11. 12..
 */
public abstract class BasePresenter<T> {

    @Getter
    protected final Context context;

    @Getter
    private final T view;

    protected BasePresenter(Context context, T view) {
        this.context = context;
        this.view = view;
        inject(BaseApplication.getInstance().getApplicationComponent());
    }

    protected abstract void inject(ApplicationComponent applicationComponent);
}
