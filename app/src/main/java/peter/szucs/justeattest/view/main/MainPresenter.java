package peter.szucs.justeattest.view.main;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import peter.szucs.justeattest.R;
import peter.szucs.justeattest.data.ApiManager;
import peter.szucs.justeattest.data.LocationManager;
import peter.szucs.justeattest.di.ApplicationComponent;
import peter.szucs.justeattest.view.base.BasePresenter;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * MainPresenter deals with all the UI logic and data requests.
 * Created by peter on 2015. 11. 12..
 */
public class MainPresenter extends BasePresenter<MainView> {

    @Inject
    ApiManager apiManager;

    @Inject
    LocationManager locationManager;

    public MainPresenter(Context context, MainView view) {
        super(context, view);
    }

    @Override
    protected void inject(ApplicationComponent applicationComponent) {
        applicationComponent.inject(this);
    }

    public void requestData(String outcode) {
        apiManager.getRestaurantsForOutcode(outcode)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(getView()::showProgress)
                .doOnUnsubscribe(getView()::hideProgress)
                .subscribe(getView()::dataArrived,
                        throwable -> {
                            getView().showMessage(throwable.getMessage());
                            throwable.printStackTrace();
                        });
    }

    public void onClickGetMyPostCode() {
        Location lastLocation = locationManager.getLastLocation();
        if (lastLocation != null) {
            if (Geocoder.isPresent())  {
                Geocoder geocoder = new Geocoder(context);

                try {
                    List<Address> addresses = geocoder.getFromLocation(lastLocation.getLongitude(), lastLocation.getLongitude(), 0);

                    if (!addresses.isEmpty()) {
                        String postalCode = addresses.get(0).getPostalCode();
                        getView().setPostalCodeFromLocation(postalCode);
                    } else {
                        getView().showMessage(context.getString(R.string.no_addresses_found));
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
