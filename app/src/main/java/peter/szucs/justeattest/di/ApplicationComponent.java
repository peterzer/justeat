package peter.szucs.justeattest.di;

import dagger.Component;
import peter.szucs.justeattest.view.main.MainPresenter;

/**
 * Where injections happen
 * Created by peterszucs on 10/11/15.
 */
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    // Presenter Layer Injection
    void inject(MainPresenter mainPresenter);

}
