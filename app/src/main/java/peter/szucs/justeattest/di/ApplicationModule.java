package peter.szucs.justeattest.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import peter.szucs.justeattest.BaseApplication;
import peter.szucs.justeattest.data.ApiManager;
import peter.szucs.justeattest.data.LocationManager;

/**
 * Provides all the injections
 * Created by peterszucs on 10/11/15.
 */
@Module
public class ApplicationModule {

    private final BaseApplication application;

    public ApplicationModule(BaseApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context providesBaseContext() {
        return application.getApplicationContext();
    }

    @Provides
    public ApiManager providesApiManager() {
        return new ApiManager(application.getApplicationContext());
    }

    @Provides
    public LocationManager providesLocationManager() {
        return new LocationManager(application.getApplicationContext());
    }

}
